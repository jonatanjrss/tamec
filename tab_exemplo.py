from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.graphics.context_instructions import Color

from kivymd.theming import ThemeManager, ThemableBehavior
from kivymd.uix.tab import MDTabsBase


main_widget_kv = '''

RootExample

<RootExample>
    orientation: 'vertical'
    MDTabs:
        id: android_tabs
        MyTab
            text: 'Tab 1'
        MyTab:
            text: 'Tab 2'

<MyTab>
'''

class MyTab(MDTabsBase):
    pass


class RootExample(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Veja abaixo 2 métodos possíveis para muda cor do MDTabBar para preto

        # método 1 (descomente a linha abaixo)
        #self.ids.android_tabs.ids.tab_bar.canvas.children[0].rgba = [0,0,0,1]

        # método 2 (descomente as linhas abaixo)
        #for i in self.ids.android_tabs.ids.tab_bar.canvas.children:
        #    if isinstance(i, Color):
        #        i.rgba = [0,0,0,1]
        #        break


class ExampleApp(App):
    theme_cls = ThemeManager()
    def build(self):
        return Builder.load_string(main_widget_kv)


ExampleApp().run()
