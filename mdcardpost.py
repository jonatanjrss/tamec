from kivy.app import App
from kivy.lang import Builder
from kivymd.theming import ThemeManager
from kivymd.toast import toast
from kivy.factory import Factory
from kivymd.uix.card import MDCardPost


Builder.load_string('''
<ExampleCardPost@BoxLayout>
    ScrollView:
        id: scroll
        size_hint: 1, 1
        do_scroll_x: False

        GridLayout:
            id: grid_card
            cols: 1
            spacing: dp(5)
            padding: dp(5)
            size_hint_y: None
            height: self.minimum_height
'''
)


class Example(App):
    theme_cls = ThemeManager()

    def build(self):
        self.screen = Factory.ExampleCardPost()
        return self.screen

    def on_start(self):
        def callback_for_menu_items(text_item):
            #coloque aqui o que você deseja fazer quando o item do menu for acionado
            print('Item 1 presionado')

        #def callback_for_menu_items_2(text_item):
        #    print('Item 2 presionado')

        #acessa o id grid_card pois dentro do GridLayout serão criados os MDCardPost
        instance_grid_card = self.screen.ids.grid_card
        
        
        # recebe uma lista de dicionários, cada dicionário será um item dentro do menu
        menu_items = [
            {'viewclass': 'MDMenuItem',
             'text': 'Fazer alguma coisa',
             'callback': callback_for_menu_items},
             #{'viewclass': 'MDMenuItem',
             #'text': 'Fazer alguma outra coisa',
             #'callback': callback_for_menu_items_2}
            ]

        #cria o MDCardPost
        instance_grid_card.add_widget(
            MDCardPost(
                right_menu=menu_items, swipe=True,
                text_post='Card with a button to open the menu MDDropDown',
                ))

Example().run()
